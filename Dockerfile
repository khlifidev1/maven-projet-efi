###stage Build
FROM maven:3-jdk-8-slim as build

WORKDIR /app

COPY . .

RUN mvn clean install package 

###stage RUN
FROM tomcat 

COPY --from=build    /app/webapp/target/webapp.war   /usr/local/tomcat/webapps 
